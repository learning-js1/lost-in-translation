//Using local storage to save Username
export const setStorage = (key, value) => {

    const json = JSON.stringify(value);
    localStorage.setItem(key, json);
    console.log(key, ": ", value);

}

//Using local storage to retrieve Username
export const getStorage = (key) => {
    const storedValue = localStorage.getItem(key);
    if (!storedValue) return false;
    console.log("Key: ",key);
    return JSON.parse(storedValue);
}