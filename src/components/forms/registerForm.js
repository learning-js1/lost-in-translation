import React, { useState } from "react";

const RegisterForm = props => {

  const [username, setUsername] = useState("");

  const onRegisterClicked = () => {
    props.complete(username);
  }

  const onUsernameChange = event => setUsername(event.target.value.trim());


  return (
    <div class="input-group mb-3">
      <input
        type="text"
        class="form-control"
        placeholder="What's your name?"
        onChange={onUsernameChange}
      />
      <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="button" onClick= { onRegisterClicked }>
          Enter
        </button>
      </div>
    </div>
  );
};

export default RegisterForm;
