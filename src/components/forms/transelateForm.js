import React, { useState } from "react";

const TranselateForm = props => {

    const [words, setWords] = useState("");

    const onTranselateClicked = () => {
        props.complete(words);
    }

    const onWordsChange = event => setWords(event.target.value.trim());


    return (
        <div class="input-group mb-3">
            <input
                type="text"
                class="form-control"
                placeholder="Get started translating!" 
                onChange={onWordsChange}
                />
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" onClick={onTranselateClicked}>Enter</button>
            </div>
        </div>
    );
};

export default TranselateForm;