import React from "react";
import RegisterForm from "../forms/registerForm";
import { useHistory } from 'react-router-dom';
import { setStorage } from "../../utils/localStorage";

const Register = () => {

  const history = useHistory();

  const handleRegisterComplete = (result) => {
    if (result){
      setStorage("Username", result);
      setStorage("Transelated Words", []);
      history.replace("/transelate");
    }
  }

  return (
    <div class="bg-warning text-light">
      <div class="row"> 
        <div class="col"></div>
        <div class="col-6">
          <h1 class="display-3">Lost In Translation</h1>
          <p>Get Started</p>
          <RegisterForm complete={handleRegisterComplete} />
        </div>
        <div class="col"></div>
      </div>
    </div>
  );
};

export default Register;
