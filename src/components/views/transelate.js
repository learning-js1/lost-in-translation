import React from "react";
import { getStorage, setStorage } from "../../utils/localStorage";
import TranselateForm from "../forms/transelateForm";

const Transelate = () => {


  // const showImages = () => {

  //   const img = JSON.parse(localStorage.getItem("Transelated Words"));

  //   return (
  //   <li><img></img>Image</li>
  //     );
  // };

  const handleTranselateComplete = (result) => {
    result = result.split("");
    let images = [];

    result.forEach((char) => {
      if (char !== " ")
        images.push(char.toLowerCase());
    });

    let saved = JSON.parse(localStorage.getItem("Transelated Words"));
    console.log(typeof(saved));
    saved.push(images);
    setStorage("Transelated Words", JSON.stringify(saved));
    console.log(saved);
    return images;
  };

  return (
    <div class="jumbotron">
      <h1 class="display-4">Lost in Translation!</h1>
      <p class="lead">Get started</p>
      <TranselateForm complete={handleTranselateComplete} />
      <div id="images">
        <h4>Transelation</h4>
      </div>
      <div class="output">
        <h4>Outputed images</h4>
        <ul>
          <li> test</li>
        </ul>
      </div>
    </div>
  );
};

export default Transelate;
