import React from "react";

const Navbar = () => {
  return (
    <div class="border-bottom border-secondary">
      <nav class="navbar navbar-light bg-warning">
        <span class="navbar-brand mb-0 h1 text-light" id="navbar">Lost In Translation</span>
      </nav>
    </div>
  );
};

export default Navbar;
