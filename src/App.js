import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Register from "./components/views/register";
import Navbar from "./components/navbar";
import Transelate from "./components/views/transelate";
import Profile from "./components/views/profile";
import NotFound from "./components/views/NotFound";
import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/register" component={Register} />
          <Route path="/transelate" component={Transelate} />
          <Route path="/profile" component={Profile} />
          <Route path="*" component = { NotFound} />
        </Switch>
      </div>
    </Router>

  );
}

export default App;
